package com.albasheep.statbde;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.albasheep.statbde.ClassModel.Produit;
import com.albasheep.statbde.ClassModel.TypeProduit;

import java.lang.ref.WeakReference;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;


/**
 * Created by Alex Chopard on 02/11/2017.
 */

public class AjouterProduitActivity extends Activity {
    TextView text;
    RadioGroup rg;
    Button btn_ajouter;
    EditText nomPdt;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(MainActivity.estConnecter)
            setContentView(R.layout.activity_ajouterproduit);
        else
            setContentView(R.layout.activity_noconnecter);

        text = (TextView) findViewById(R.id.erreur);
        rg = (RadioGroup) findViewById(R.id.rg_typeproduit);
        btn_ajouter = (Button) findViewById(R.id.btn_ajouter);

        nomPdt = (EditText) findViewById(R.id.et_nomProduit);

        LoadTypeProduitTask loadTypeProduitTask = new LoadTypeProduitTask(AjouterProduitActivity.this);
        loadTypeProduitTask.execute();


        btn_ajouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ajoutePdt();
            }
        });

        nomPdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // Quand on appuis sur OK
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    ajoutePdt();
                }
                return false;
            }
        });

    }

    public void ajoutePdt(){
        Produit pdt = new Produit();


        if(rg.getCheckedRadioButtonId() != -1){
            if(!nomPdt.getText().toString().equals("")){
                CheckBox disponiblePdt = (CheckBox) findViewById(R.id.cb_disponible);

                pdt.setNomProduit(nomPdt.getText().toString());
                pdt.setDisponible(disponiblePdt.isChecked());
                pdt.setIdTypeProduit(rg.getCheckedRadioButtonId());

                InsertProduitTask insert = new InsertProduitTask(AjouterProduitActivity.this, pdt);
                insert.execute();
            } else{
                err("Renseignez le nom du produit !");
            }

        } else{
            err("Séléctionnez le type du produit !");
        }
    }

    public void loadTypeProduitPage(ArrayList<TypeProduit> lesTypeProduits){

        for(int i = 0; i < lesTypeProduits.size(); i++){
            RadioButton rb = new RadioButton(this);
            rb.setText(lesTypeProduits.get(i).getNomTypeProduit());
            rb.setId(lesTypeProduits.get(i).getIdTypeProduit());

            rg.addView(rb);
        }

    }

    // L'ajoute est bon, on termine l'activité
    public void retour(){
        this.finish();
    }

    public void err(String e){
        text.setText(e);
    }

    // Charge les Type de Produit
    static class LoadTypeProduitTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<AjouterProduitActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        ArrayList<TypeProduit> typeProduits;
        String t;

        public LoadTypeProduitTask (AjouterProduitActivity pActivity) {
            link(pActivity);
            typeProduits = new ArrayList<>();
            t = "";
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                if(mActivity.get() != null)
                    mActivity.get().loadTypeProduitPage(typeProduits);
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();

                // Récupère les types de produits
                result = state.executeQuery("select * from vTypeProduit;");
                resultMeta = result.getMetaData();
                while(result.next()){
                    typeProduits.add(new TypeProduit(Integer.parseInt(result.getObject(1).toString()), result.getObject(2).toString()));
                }

                result.close();
                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (AjouterProduitActivity pActivity) {
            mActivity = new WeakReference<AjouterProduitActivity>(pActivity);
        }
    }

    // Inser un nouveau produit dans la base de donnée
    static class InsertProduitTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<AjouterProduitActivity> mActivity = null;
        Statement state;
        String t;
        Produit produit;

        public InsertProduitTask (AjouterProduitActivity pActivity, Produit iProduit) {
            link(pActivity);
            produit = iProduit;
            t = "";
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                Toast.makeText(mActivity.get(), produit.getNomProduit() + " ajouter",Toast.LENGTH_SHORT).show();
                if(mActivity.get() != null)
                    mActivity.get().retour();
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();
                // Insertion du nouveu produit
                state.executeQuery("select ajouteProduit(" + produit.getIdTypeProduit() + ", '" + produit.getNomProduit() + "', " + produit.getDisponible() + ");");

                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (AjouterProduitActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }
}
