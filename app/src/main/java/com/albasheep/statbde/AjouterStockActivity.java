package com.albasheep.statbde;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.albasheep.statbde.ClassModel.Produit;
import com.albasheep.statbde.ClassModel.Stock;

import java.lang.ref.WeakReference;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;


/**
 * Created by Alex Chopard on 03/11/2017.
 */

public class AjouterStockActivity extends Activity {
    TextView text;
    ArrayList<Produit> aProduits;
    Spinner spinner;
    Button btn_ajouter;
    EditText quantite;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(MainActivity.estConnecter)
            setContentView(R.layout.activity_ajouterstock);
        else
            setContentView(R.layout.activity_noconnecter);

        text = (TextView) findViewById(R.id.erreur);
        spinner = (Spinner) findViewById(R.id.spi_produit);
        btn_ajouter = (Button) findViewById(R.id.btn_ajouterNouveauStock);
        quantite = (EditText) findViewById(R.id.et_quantite);

        aProduits = new ArrayList<>();

        LoadProduitTask loadProduitTask = new LoadProduitTask(AjouterStockActivity.this);
        loadProduitTask.execute();

        btn_ajouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ajouteStock();
            }
        });

        quantite.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // Quand on appuis sur OK
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    ajouteStock();
                }
                return false;
            }
        });

    }

    public void ajouteStock(){
        String nomPdt = spinner.getSelectedItem().toString();
        // On regarde si on a bien sélécionné un produit
        if(!nomPdt.equals("-- aucun --")){
            // On regarde si on a bien séléctionné une quantité
            if(!quantite.getText().toString().equals("")){
                Stock stock = new Stock();
                stock.setQuantite(Integer.parseInt(quantite.getText().toString()));

                for(int i = 0; i < aProduits.size(); i++){
                    if(nomPdt.equals(aProduits.get(i).getNomProduit()))
                        stock.setIdProduit(aProduits.get(i).getIdProduit());
                }

                InsertStockTask insertStockTask = new InsertStockTask(AjouterStockActivity.this, stock);
                insertStockTask.execute();

                spinner.setSelection(0);
                quantite.setText("");
                err("");
            } else{
                err("Veuillez séléctioner une quantité.");
            }
        } else{
            err("Veuillez séléctioner un produit.");
        }
    }

    public void loadProduitPage(ArrayList<Produit> lesProduits){
        aProduits = lesProduits;
        ArrayList<String> listProduit = new ArrayList<>();
        listProduit.add("-- aucun --");

        for(int i = 0; i < aProduits.size(); i++)
            listProduit.add(aProduits.get(i).getNomProduit());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, listProduit);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
    }

    // L'ajoute est bon, on termine l'activité
    public void retour(){
        //this.finish();
    }
    public void err(String e){
        text.setText(e);
    }

    static class LoadProduitTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<AjouterStockActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        ArrayList<Produit> produits;
        String t;

        public LoadProduitTask (AjouterStockActivity pActivity) {
            link(pActivity);
            produits = new ArrayList<>();
            t = "";
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                if(mActivity.get() != null)
                    mActivity.get().loadProduitPage( produits);
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();

                // Récupère tout les produits
                result = state.executeQuery("select * from vProduit");
                resultMeta = result.getMetaData();
                while(result.next()){
                    produits.add(new Produit(Integer.parseInt(result.getObject(1).toString()),
                            Integer.parseInt(result.getObject(2).toString()),
                            Integer.parseInt(result.getObject(3).toString()),
                            result.getObject(4).toString(),
                            Boolean.parseBoolean(result.getObject(5).toString()),
                            result.getObject(6).toString(),
                            Integer.parseInt(result.getObject(7).toString()),
                            0
                    ));
                }

                result.close();
                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (AjouterStockActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }

    // Inser un nouveau stock dans la base de donnée
    static class InsertStockTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<AjouterStockActivity> mActivity = null;
        Statement state;
        String t;
        Stock stock;

        public InsertStockTask (AjouterStockActivity pActivity, Stock iStock) {
            link(pActivity);
            stock = iStock;
            t = "";
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                Toast.makeText(mActivity.get(), "Stock de ajouter",Toast.LENGTH_SHORT).show();
                if(mActivity.get() != null)
                    mActivity.get().retour();
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();

                // Insertion du nouveu produit
                state.executeQuery("select ajouteStock(" + stock.getIdProduit() + ", " + stock.getQuantite() + ");");

                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (AjouterStockActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }
}
