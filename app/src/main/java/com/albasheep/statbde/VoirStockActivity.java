package com.albasheep.statbde;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.albasheep.statbde.ClassModel.Produit;

import java.lang.ref.WeakReference;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;



/**
 * Created by Alex Chopard on 02/11/2017.
 */

public class VoirStockActivity extends Activity {
    TextView text;
    LinearLayout ll;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(MainActivity.estConnecter)
            setContentView(R.layout.activity_voirstock);
        else
            setContentView(R.layout.activity_noconnecter);

        text = (TextView) findViewById(R.id.erreur);
        ll = (LinearLayout) findViewById(R.id.ll_affichage);

        LoadStockTask loadStockTask = new LoadStockTask(VoirStockActivity.this);
        loadStockTask.execute();

    }

    public void loadQuantitePage(ArrayList<Produit> lesProduit){
        // On créer le layoutParametre qui est commmun à toute les views
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);

        for(int i = 0; i < lesProduit.size(); i++){
            int quantite = lesProduit.get(i).getQuantite();
            //On créer un layout hotizontale pour contenir le produit
            LinearLayout llh = new LinearLayout(this);
            llh.setOrientation(LinearLayout.HORIZONTAL);
            llh.setLayoutParams(lp);
            llh.setPadding(10, 20, 10, 10);

            TextView nom = new TextView(this);
            nom.setText(lesProduit.get(i).getNomProduit());
            nom.setTextSize(15);

            if(quantite < 5){
                nom.setTextColor(getResources().getColor(R.color.rouge));
            }else if(quantite < 20){
                nom.setTextColor(getResources().getColor(R.color.orange));
            } else {
                nom.setTextColor(getResources().getColor(R.color.colorVerte));
            }

            TextView qte = new TextView(this);
            qte.setText(lesProduit.get(i).getQuantite() + " unitée(s)");
            nom.setTextSize(15);
            qte.setGravity(Gravity.END);

            llh.addView(nom, lp);
            llh.addView(qte, lp);

            LinearLayout barre = new LinearLayout(this);
            barre.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1, 1));
            barre.setBackgroundColor(getResources().getColor(R.color.rouge));

            ll.addView(llh);
            ll.addView(barre);
        }
    }

    public void err(String e){
        text.setText(e);
    }

    // Charge les Produits en vente avec leurs quantité en stock
    static class LoadStockTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<VoirStockActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        ArrayList<Produit> produits;
        String t;

        public LoadStockTask (VoirStockActivity pActivity) {
            link(pActivity);
            produits = new ArrayList<>();
            t = "";
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                if(mActivity.get() != null)
                    mActivity.get().loadQuantitePage(produits);
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();

                // Récupère tout les produits
                result = state.executeQuery("select * from vPRoduitAvecQte;");
                resultMeta = result.getMetaData();
                while(result.next()){
                    produits.add(new Produit(Integer.parseInt(result.getObject(1).toString()),
                            Integer.parseInt(result.getObject(2).toString()),
                            Integer.parseInt(result.getObject(3).toString()),
                            result.getObject(4).toString(),
                            Boolean.parseBoolean(result.getObject(5).toString()),
                            result.getObject(6).toString(),
                            Integer.parseInt(result.getObject(7).toString()),
                            Integer.parseInt(result.getObject(7).toString())
                    ));
                }

                result.close();
                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (VoirStockActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }
}
