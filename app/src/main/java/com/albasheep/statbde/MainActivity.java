package com.albasheep.statbde;

/**
 * Created by Alex Chopard on 02/11/2017.
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity {

    CheckPermission checkPermission;
    Button btnConn;
    TextView err;
    EditText mdp;
    public static boolean estConnecter = false;
    MessageDigest md;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);


        // Onrécupère l'edittext qui contient le mdp
        mdp = (EditText) findViewById(R.id.et_mdp);

        btnConn = (Button) findViewById(R.id.btnConn);
        err = (TextView) findViewById(R.id.erreur);

        // initialisation demande permission
        checkPermission = new CheckPermission(MainActivity.this, this);
        // On regarde si on à toute les permissions
        checkPermission.checkAllPermission();

        btnConn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkMdp();
            }
        });

        mdp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // Quand on appuis sur OK
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    checkMdp();
                }
                return false;
            }
        });


    }

    public String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public void checkMdp(){
        // On récupère le mdp
        String smdp = mdp.getText().toString();

        // Hash du mdp
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(smdp.getBytes());

            smdp = bytesToHexString(md.digest());
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }

        // Test si le mdp est bon
        if(smdp.equals("b2133cc3beef2779a324891c7cf4ba62f26f7108093cb2d3394ab1b0998f4ae3")){
            mdp.setText("");
            estConnecter = true;
            // Si le mdp est bon, on lance l'activité
            Intent intent = new Intent(MainActivity.this, MenuPrincipaleActivity.class);
            startActivity(intent);
        }else{
            // Si le mdp est incorrect on affiche un message d'erreur et on vide le champs mdp
            err.setText(R.string.err_login);
            mdp.setText("");
        }
    }
}
