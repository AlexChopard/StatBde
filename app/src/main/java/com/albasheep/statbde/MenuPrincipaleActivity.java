package com.albasheep.statbde;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Alex Chopard on 15/11/2017.
 */

public class MenuPrincipaleActivity extends AppCompatActivity {

    Button btnVente, btnproduit, btnstock, btnstatistique;
    TextView statuCo, tv_rechargerco;
    Animation anim;
    ImageView ivChargement;
    public static Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // On regarde si la personne est vraiment connecter (mis pour empêcher le saut de la page de connexion
        if(MainActivity.estConnecter)
            setContentView(R.layout.activity_main);
        else
            setContentView(R.layout.activity_noconnecter);

        context = getBaseContext();


        // On récupère les boutons
        btnVente = (Button) findViewById(R.id.btn_vente);
        btnproduit = (Button) findViewById(R.id.btn_produits);
        btnstock = (Button) findViewById(R.id.btn_stock);
        btnstatistique = (Button) findViewById(R.id.btn_statistique);


        ivChargement = (ImageView) findViewById(R.id.iv_chargement_bdd);
        anim = AnimationUtils.loadAnimation(this, R.anim.anim_rotation);
        statuCo = (TextView) findViewById(R.id.tv_statuConnexion);
        tv_rechargerco = (TextView) findViewById(R.id.tv_rechargerCo);


        // On met l'affichage comme quoi on recherche la connexion
        initAffichageStatuConnexion();
        // début de l'animation de chargement de la connexion
        ivChargement.startAnimation(anim);
        // Création de la connexion
        ConnexionBDD.instance();


        btnVente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuPrincipaleActivity.this, VenteActivity.class);
                startActivity(intent);
            }
        });

        btnproduit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(MenuPrincipaleActivity.this, GestionProduitActivity.class);
                startActivity(intent);
            }
        });

        btnstock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuPrincipaleActivity.this, GestionStockActivity.class);
                startActivity(intent);
            }
        });

        btnstatistique.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuPrincipaleActivity.this, GraphcventparproduitActivity.class);
                startActivity(intent);
            }
        });


        statuCo.setOnClickListener(clickRechargerCo);
        tv_rechargerco.setOnClickListener(clickRechargerCo);

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        ConnexionBDD.instance().creerCo();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_main :
                Intent intent = new Intent(MenuPrincipaleActivity.this, PreferenceMenuActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // Envent on click pour recharger la connexion
    private View.OnClickListener clickRechargerCo = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(statuCo.getText().toString().compareTo(getResources().getString(R.string.connecte)) == 0){
                ConnexionBDD.instance().creerCo();
            }
        }
    };


    // fonction pour initaliser quand la connexion est pas ok
    public void initAffichageStatuConnexion(){
        ivChargement.setVisibility(View.VISIBLE);
        tv_rechargerco.setVisibility(View.INVISIBLE);
        statuCo.setText(R.string.connexion_cour);
        statuCo.setTextColor(getResources().getColor(R.color.connexionNonOK));
    }

    // Quand la connexion est OK
    public void connexionOk(){
        // On enlève l'animation
        ivChargement.clearAnimation();
        ivChargement.setVisibility(View.INVISIBLE);
        tv_rechargerco.setVisibility(View.VISIBLE);
        statuCo.setText(R.string.connecte);
        statuCo.setTextColor(getResources().getColor(R.color.connexionOK));
    }

}
