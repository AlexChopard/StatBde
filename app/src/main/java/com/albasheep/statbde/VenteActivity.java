package com.albasheep.statbde;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.albasheep.statbde.ClassModel.Produit;
import com.albasheep.statbde.ClassModel.TypeProduit;

import java.lang.ref.WeakReference;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;


/**
 * Created by Alex Chopard on 23/10/2017.
 */

public class VenteActivity extends Activity {
    // connexion à la base de donnée

    private int NOMBRE_PRODUIT_PAR_LIGNE = 2;
    private static int SECOND = 1000;
    private static int CONDITION_ORANGE = 20;
    private static int CONDITION_ROUGE = 1;

    LoadTask loadTask;
    ArrayList<TypeProduit> typeProduits;
    public static ArrayList<Produit> produits;
    public static ArrayList<Button> lesBoutons;
    ArrayList<ScrollView> scrollViews;
    LinearLayout  linearLayout;
    FrameLayout mainLayour;
    TextView text;
    public static Context context;
    public static Activity activity;
    Animation animation_button;

    private boolean afficheimage = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(MainActivity.estConnecter)
            setContentView(R.layout.activity_vente);
        else
            setContentView(R.layout.activity_noconnecter);


        lesBoutons = new ArrayList<>();
        produits = new ArrayList<>();
        scrollViews = new ArrayList<>();
        context = getApplicationContext();
        activity = this;
        animation_button = AnimationUtils.loadAnimation(this, R.anim.anim_button);

        linearLayout = (LinearLayout) findViewById(R.id.linear);
        mainLayour = (FrameLayout) findViewById(R.id.main_layout);
        text = (TextView) findViewById(R.id.textView);


        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        afficheimage = pref.getBoolean("cbafficheimage", false);

        // definit le nombre de pdt par ligne en fonction de si y a les images ou pas
        NOMBRE_PRODUIT_PAR_LIGNE = (afficheimage) ? 3: 2;

        // On charge la BDD
        loadTask = new LoadTask(VenteActivity.this);
        loadTask.execute();

        // On lance le service qui met a jour les stock toute les minutes
        /*Intent majService = new Intent(VenteActivity.this, MajProduitService.class);
        //startService(majService);
        PendingIntent pendingIntent = PendingIntent.getService(VenteActivity.this, 0, majService, 0);

        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        long frequency = 30 * SECOND;
        alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), frequency, pendingIntent);*/

    }


    public void loadPage(ArrayList<TypeProduit> list, ArrayList<Produit> list2){
        typeProduits = list;
        produits = list2;

        for(int i = 0; i < typeProduits.size(); i++){
            int nbHori = 0;
            //Bouton pour les TypeProduits
            Button btn = new Button(this);
            // Bar de scroll qui contiendera les produit du type
            final ScrollView scrollView = new ScrollView(this);
            scrollView.setId(typeProduits.get(i).getIdTypeProduit());
            // Linear layout de la scollView
            LinearLayout ll = new LinearLayout(this);
            ll.setOrientation(LinearLayout.VERTICAL);

            LinearLayout llh = new LinearLayout(this);
            llh.setOrientation(LinearLayout.HORIZONTAL);
            // Ajoute les différents produit du type
            for(int y = 0; y < produits.size(); y++){
                if(produits.get(y).getIdTypeProduit() == typeProduits.get(i).getIdTypeProduit()) {
                    int quantite = produits.get(y).getQuantite();

                    LinearLayout l = new LinearLayout(this);
                    l.setPadding(5, 5, 5, 5);
                    LinearLayout.LayoutParams para = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                    LinearLayout.LayoutParams paraImg = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200, 1);

                    l.setLayoutParams(para);

                    if(produits.get(y).getImageProduit().compareTo("null") == 0 || !afficheimage){
                        // Si pas image
                        Button btn2 = new Button(this);
                        btn2.setLayoutParams(para);
                        btn2.setText(produits.get(y).getNomProduit());
                        btn2.setId(produits.get(y).getIdProduit());
                        btn2.setTextSize(15);
                        btn2.setPadding(0, 60, 0, 60);

                        lesBoutons.add(btn2);

                        l.addView(btn2);

                        if(quantite < CONDITION_ROUGE){
                            btn2.setBackgroundColor(getResources().getColor(R.color.rougeT));
                        }else if(quantite < CONDITION_ORANGE){
                            btn2.setBackgroundColor(getResources().getColor(R.color.orangeT));
                        } else {
                            btn2.setBackgroundColor(getResources().getColor(R.color.verteT));
                        }


                        btn2.setOnClickListener(clickProduit);
                        btn2.setOnLongClickListener(longClickProduit);
                    }else{
                        // Si image
                        ImageView img = new ImageView(this);
                        img.setLayoutParams(paraImg);
                        img.setId(produits.get(y).getIdProduit());

                        int id = getResources().getIdentifier(produits.get(y).getImageProduit(), "drawable", getPackageName());
                        img.setImageResource(id);

                        // On definit la couleur en fonction de la quantité en stock
                        if(quantite < CONDITION_ROUGE){
                            img.setBackgroundColor(getResources().getColor(R.color.rougeT));
                        }else if(quantite < CONDITION_ORANGE){
                            img.setBackgroundColor(getResources().getColor(R.color.orangeT));
                        } else {
                            img.setBackgroundColor(getResources().getColor(R.color.verteT));
                        }

                        l.addView(img);

                        img.setOnClickListener(clickProduit);
                        img.setOnLongClickListener(longClickProduit);

                    }


                    if(nbHori == 0) {
                        llh = new LinearLayout(this);
                        llh.setOrientation(LinearLayout.HORIZONTAL);
                        llh.setLayoutParams(para);
                        ll.addView(llh);
                    }

                    llh.addView(l);
                    nbHori ++;
                    if(nbHori == NOMBRE_PRODUIT_PAR_LIGNE)
                        nbHori = 0;



                }
            }
            // Ajoute le Scroll à la View
            scrollView.addView(ll);
            scrollView.setVisibility(View.GONE);
            //On ajoute le scrollView a la site des scrollViews
            scrollViews.add(scrollView);
            mainLayour.addView(scrollView);
            // Création du bouton pour les types
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
            btn.setLayoutParams(lp);
            btn.setText(typeProduits.get(i).getNomTypeProduit());
            btn.setTextSize(25);
            //btn.setId(typeProduits.get(i).getIdTypeProduit());
            linearLayout.addView(btn);
            linearLayout.setVisibility(View.VISIBLE);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayout.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);

                }
            });
        }
    }

    private View.OnClickListener clickProduit = new View.OnClickListener() {
        int quantite, indice;

        @Override
        public void onClick(View v) {
            v.startAnimation(animation_button);
            for(int y = 0; y < produits.size(); y++)
                if(produits.get(y).getIdProduit() == v.getId()) {
                    quantite = produits.get(y).getQuantite();
                    produits.get(y).setQuantite(quantite - 1);
                    indice = y;
                }

            if(quantite < CONDITION_ROUGE){
                v.setBackgroundColor(getResources().getColor(R.color.rougeT));
            }else if(quantite < CONDITION_ORANGE){
                v.setBackgroundColor(getResources().getColor(R.color.orangeT));
            } else {
                v.setBackgroundColor(getResources().getColor(R.color.verteT));
            }

            InsertAchatTask insert = new InsertAchatTask(VenteActivity.this, v.getId(), indice);
            insert.execute();
        }
    };

    private View.OnLongClickListener longClickProduit = new View.OnLongClickListener() {
        int quantite, indice;

        @Override
        public boolean onLongClick(View v) {
            v.startAnimation(animation_button);
            for(int y = 0; y < produits.size(); y++)
                if(produits.get(y).getIdProduit() == v.getId()) {
                    quantite = produits.get(y).getQuantite();
                    produits.get(y).setQuantite(quantite - 2);
                    indice = y;
                }

            if(quantite < CONDITION_ROUGE){
                v.setBackgroundColor(getResources().getColor(R.color.rougeT));
            }else if(quantite < CONDITION_ORANGE){
                v.setBackgroundColor(getResources().getColor(R.color.orangeT));
            } else {
                v.setBackgroundColor(getResources().getColor(R.color.verteT));
            }

            InsertAchat2Task insert = new InsertAchat2Task(VenteActivity.this, v.getId(), indice);
            insert.execute();

            return true;
        }
    };

    public static void majPdt (ArrayList<Produit> nouveauPdt) {
        Log.i("majPdt", "nouveau produit charger !");
        int quantite = 0;
        if(produits.size() == nouveauPdt.size() || true){
            int tailleListBtn = lesBoutons.size();
            Button btn;
            produits = nouveauPdt;
            for (int i = 0; i < produits.size(); i++){
                btn = null;
                if(produits.get(i).getDisponible()){
                    for(int y = 0; y < tailleListBtn; y ++){
                        if (lesBoutons.get(y).getId() == produits.get(i).getIdProduit()){
                            btn = lesBoutons.get(y);
                            y = tailleListBtn;
                        }
                    }
                    quantite = produits.get(i).getQuantite();

                    if(btn != null) {
                        if(quantite < CONDITION_ROUGE){
                            btn.setBackgroundColor(context.getResources().getColor(R.color.rougeT));
                        }else if(quantite < CONDITION_ORANGE){
                            btn.setBackgroundColor(context.getResources().getColor(R.color.orangeT));
                        } else {
                            btn.setBackgroundColor(context.getResources().getColor(R.color.verteT));
                        }
                    }
                }
            }
        }
    }

    public void err(String e){
        text.setText(e);
    }

    @Override
    public void onBackPressed() {
        if(linearLayout.getVisibility() == View.VISIBLE){
            super.onBackPressed();
        }else{
            linearLayout.setVisibility(View.VISIBLE);
            for(int i = 0; i < scrollViews.size(); i++)
                scrollViews.get(i).setVisibility(View.GONE);
        }
    }

    // Charge les Produits en vente avec leur quantité
    static class LoadTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<VenteActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        ArrayList<TypeProduit> typeProduits;
        ArrayList<Produit> produits;
        String t;

        public LoadTask (VenteActivity pActivity) {
            link(pActivity);
            typeProduits = new ArrayList<>();
            produits = new ArrayList<>();
            t = "";
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                if(mActivity.get() != null)
                    mActivity.get().loadPage(typeProduits, produits);
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();
                // Récupère les types de produits
                result = state.executeQuery("select * from vTypeProduit");
                resultMeta = result.getMetaData();
                while(result.next()){
                    typeProduits.add(new TypeProduit(Integer.parseInt(result.getObject(1).toString()), result.getObject(2).toString()));
                }
                // Récupère tout les produits
                result = state.executeQuery("select * from vProduitAvecQte");
                resultMeta = result.getMetaData();
                while(result.next()){
                    produits.add(new Produit(Integer.parseInt(result.getObject(1).toString()),
                            Integer.parseInt(result.getObject(2).toString()),
                            Integer.parseInt(result.getObject(3).toString()),
                            result.getObject(4).toString(),
                            Boolean.parseBoolean(result.getObject(5).toString()),
                            result.getObject(6).toString(),
                            Integer.parseInt(result.getObject(8).toString()),
                            Integer.parseInt(result.getObject(7).toString())
                    ));
                }

                result.close();
                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (VenteActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }

    // Inser un achat dans la base de donnée
    static class InsertAchatTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<VenteActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        int idProduit, indice;
        String t;

        public InsertAchatTask (VenteActivity pActivity, int id, int ind) {
            link(pActivity);
            idProduit = id;
            t = "";
            indice = ind;
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                Toast.makeText(mActivity.get(), produits.get(indice).getNomProduit() + " x1",Toast.LENGTH_SHORT).show();
            }else{
                // Si il y a un problème on affiche le problème
                Toast.makeText(mActivity.get(), "Erreur",Toast.LENGTH_SHORT).show();
                mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();
                // Ajoute l'achat
                result = state.executeQuery("SELECT ajouteAchat(" + idProduit + ")");
                resultMeta = result.getMetaData();
                result.close();
                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (VenteActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }

    // Inser deux fois le même achat dans la base de donnée
    static class InsertAchat2Task extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<VenteActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        int idProduit, indice;
        String t;

        public InsertAchat2Task (VenteActivity pActivity, int id, int ind) {
            link(pActivity);
            idProduit = id;
            t = "";
            indice = ind;
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                Toast.makeText(mActivity.get(), produits.get(indice).getNomProduit() + " x2",Toast.LENGTH_SHORT).show();
            }else{
                // Si il y a un problème on affiche le problème
                Toast.makeText(mActivity.get(), "Erreur",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();
                // Ajoute l'achat
                result = state.executeQuery("SELECT ajouteAchat2(" + idProduit + ")");
                resultMeta = result.getMetaData();
                result.close();
                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (VenteActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }

}

