package com.albasheep.statbde;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;

import static com.albasheep.statbde.MenuPrincipaleActivity.context;

/**
 * Created by Alex Chopard on 15/11/2017.
 * Class pour créer la connexion à la base de donnée.
 */

public final class ConnexionBDD {

    private static  volatile ConnexionBDD instance = null;
    private Connection connexion = null;
    private boolean statu_connexion = false;

    // Connexion accesible dans tout le programme pour faire les requêtes.
    private static final String ipBDD = "***.***.***.***";
    private static final String port = "***";
    private static final String nomBDD = "***";
    private static final String login = "***";
    private static final String password = "***";

    private ConnexionBDD(){
        this.creerCo();
    }

    public final static ConnexionBDD instance(){
        if(ConnexionBDD.instance == null){
            synchronized (ConnexionBDD.class){
                if(ConnexionBDD.instance == null){
                    ConnexionBDD.instance = new ConnexionBDD();
                }
            }
        }
        return ConnexionBDD.instance;
    }

    public void creerCo(){
        creeConnexionTask cree = new creeConnexionTask();
        cree.execute();
    }


    public Connection getConnexion(){
        return this.connexion;
    }


    static class creeConnexionTask extends AsyncTask<Void, Integer, Boolean> {

        public creeConnexionTask () {
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                ConnexionBDD.instance().statu_connexion = true;
                Toast.makeText(context, "CONNEXION OK !!", Toast.LENGTH_SHORT).show(); // provisoir !!!!!
                //MenuPrincipaleActivity.connexionOk();
            } else{
                ConnexionBDD.instance().creerCo();
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                if(MainActivity.estConnecter){ // Si la personne est bien connecté
                    Class.forName("org.postgresql.Driver");
                    System.out.println("Driver O.K.");
                    String url = "jdbc:postgresql://" + ipBDD + ":" + port + "/" + nomBDD;
                    ConnexionBDD.instance().connexion = DriverManager.getConnection(url, login, password);

                    return true;
                }else{
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                try{
                    Thread.sleep(1000);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.i("erreur", ex.getMessage());
                }

                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
        }

        @Override
        protected void onCancelled () {
        }
    }
}
