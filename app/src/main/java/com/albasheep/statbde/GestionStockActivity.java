package com.albasheep.statbde;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

/**
 * Created by AlexChop on 04/01/2018.
 */

public class GestionStockActivity extends Activity {

    Button btnvoirstock, btnajouterstock;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gestionstock);

        btnvoirstock = (Button) findViewById(R.id.btn_voirstock);
        btnajouterstock = (Button) findViewById(R.id.btn_ajouterstock);

        btnvoirstock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GestionStockActivity.this, VoirStockActivity.class);
                startActivity(intent);
            }
        });

        btnajouterstock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GestionStockActivity.this, AjouterStockActivity.class);
                startActivity(intent);
            }
        });



    }
}
