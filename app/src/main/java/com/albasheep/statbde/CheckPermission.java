package com.albasheep.statbde;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import static android.support.v4.content.PermissionChecker.checkCallingOrSelfPermission;

/**
 * Created by Alex Chopard on 26/10/2017.
 */

public class CheckPermission {

    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 3;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE = 4;
    private static final int ALL_PERMISSONS = 5;

    private Activity activity;
    private Context context;

    public CheckPermission() {
    }

    public CheckPermission(Activity cActivity, Context cContext) {
        activity = cActivity;
        context = cContext;
    }


    public void checkAllPermission() {
        ActivityCompat.requestPermissions(activity, new String[] {
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_NETWORK_STATE}, ALL_PERMISSONS);
    }

    public void checkPermissionInternet() {
        if(!InternetIsGaranted())
            ActivityCompat.requestPermissions(activity, new String[] {Manifest.permission.INTERNET}, MY_PERMISSIONS_REQUEST_INTERNET);
    }

    public void checkPermissionAccess_Network_state() {
        if(!InternetIsGaranted())
            ActivityCompat.requestPermissions(activity, new String[] {Manifest.permission.ACCESS_NETWORK_STATE}, MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE);
    }

    public boolean InternetIsGaranted() {
        return checkCallingOrSelfPermission(context, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED;
    }
}
