package com.albasheep.statbde;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.albasheep.statbde.ClassModel.Produit;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Alex Chopard on 28/10/2017.
 */

public class MajProduitService extends IntentService {
    ArrayList<Produit> produitsMaj;
    String t;
    Statement state;
    ResultSet result;
    ResultSetMetaData resultMeta;
    int quantite;

    public MajProduitService() {
        // Il faut passer une chaîne de caractères au superconstructeur
        super("UnNomAuHasard");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
                state = ConnexionBDD.instance().getConnexion().createStatement();

                result = state.executeQuery("select * from vProduitAvecQte;");
                resultMeta = result.getMetaData();
                while(result.next()){
                    produitsMaj.add(new Produit(Integer.parseInt(result.getObject(1).toString()),
                            Integer.parseInt(result.getObject(2).toString()),
                            Integer.parseInt(result.getObject(3).toString()),
                            result.getObject(4).toString(),
                            Boolean.parseBoolean(result.getObject(5).toString()),
                            result.getObject(6).toString(),
                            Integer.parseInt(result.getObject(8).toString()),
                            Integer.parseInt(result.getObject(7).toString())
                    ));
                }
                VenteActivity.majPdt(produitsMaj);
                result.close();
                state.close();
        } catch (Exception e) {
            e.printStackTrace();
            t = e.toString();
        }
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        produitsMaj = new ArrayList<>();
        quantite = 0;
        return super.onStartCommand(intent, flags, startId);
    }

}
