package com.albasheep.statbde;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.albasheep.statbde.ClassModel.Produit;

import java.lang.ref.WeakReference;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;


/**
 * Created by AlexChop on 03/01/2018.
 */

public class DisponibiliteProduitActivity extends Activity {

    TextView err;
    ListView listView;
    ArrayList<Produit> gProduits;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(MainActivity.estConnecter)
            setContentView(R.layout.activity_disponibiliteproduit);
        else
            setContentView(R.layout.activity_noconnecter);

        gProduits = new ArrayList<>();
        err = (TextView) findViewById(R.id.tv_err);
        listView = (ListView) findViewById(R.id.listView);

        LoadProduitTask load = new LoadProduitTask(DisponibiliteProduitActivity.this);
        load.execute();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listView.isItemChecked((int) id)){
                    gProduits.get((int) id).setDisponible(true);
                }else{
                    gProduits.get((int) id).setDisponible(false);
                }

                MajDispoTask maj = new MajDispoTask(DisponibiliteProduitActivity.this, gProduits.get((int) id));
                maj.execute();

            }
        });

    }

    public void loadProduitPage(ArrayList<Produit> lesProduits){
        gProduits = lesProduits;
        ArrayList<String> element = new ArrayList<>();

        for(int i = 0; i < gProduits.size(); i++)
            element.add(gProduits.get(i).getNomProduit());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice, element);

        listView.setAdapter(adapter);

        for(int i = 0; i < gProduits.size(); i++)
            if(gProduits.get(i).getDisponible())
                listView.setItemChecked(i, true);
    }

    public void majList(Produit pdt){
        Toast.makeText(this, pdt.getNomProduit() + ((pdt.getDisponible())? " est en vente" : " n'est plus en vente"),Toast.LENGTH_SHORT).show();
    }

    public void err(String e){
        err.setText(e);
    }

    // Récupère les Produits par Type puis par nom
    static class LoadProduitTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<DisponibiliteProduitActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        ArrayList<Produit> produits;
        String t;

        public LoadProduitTask (DisponibiliteProduitActivity pActivity) {
            link(pActivity);
            produits = new ArrayList<>();
            t = "";
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                if(mActivity.get() != null)
                    mActivity.get().loadProduitPage(produits);
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();

                // Récupère tout les produits
                result = state.executeQuery("select * from vProduitParTypeNom;");
                resultMeta = result.getMetaData();
                while(result.next()){
                    produits.add(new Produit(Integer.parseInt(result.getObject(1).toString()),
                            Integer.parseInt(result.getObject(2).toString()),
                            Integer.parseInt(result.getObject(3).toString()),
                            result.getObject(4).toString(),
                            Boolean.parseBoolean(result.getObject(5).toString()),
                            result.getObject(6).toString(),
                            0, // On a pas besoin de la référence du produit
                            0 // On a pas besoin de la quantité ici, on met donc la quantité du produit à une valeur par default.
                    ));
                }

                result.close();
                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (DisponibiliteProduitActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }

    // Met à jour la disponibilité d'un Produit
    static class MajDispoTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<DisponibiliteProduitActivity> mActivity = null;
        Statement state;
        String t;
        Produit produit;

        public MajDispoTask (DisponibiliteProduitActivity pActivity, Produit mProduit) {
            link(pActivity);
            t = "";
            produit = mProduit;
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                if(mActivity.get() != null)
                    mActivity.get().majList(produit);
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();

                // Récupère tout les produits
                state.executeQuery("select updateDispoPdt(" + produit.getIdProduit() + ", " + produit.getDisponible() + ");");

                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (DisponibiliteProduitActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }

}
