package com.albasheep.statbde.ClassModel;

import android.support.annotation.NonNull;

/**
 * Created by Alex Chopard on 27/10/2017.
 */

public class Produit implements Comparable<Produit>{
    // ************ ATTRIBUT ************
    private int idProduit;
    private int idTypeProduit;
    private int idMarque;
    private String nomProduit;
    private boolean disponible;
    private String imageProduit;
    private int reference;
    private int quantite;



    public Produit(){

    }

    public Produit(int id, int idType, int idM, String nom, boolean disp, String image, int ref, int qte){
        this.setIdProduit(id);
        this.setIdTypeProduit(idType);
        this.setIdMarque(idM);
        this.setNomProduit(nom);
        this.setDisponible(disp);
        this.setImageProduit(image);
        this.setReference(ref);
        this.setQuantite(qte);
    }

    @Override
    public int compareTo(Produit o) {
        if (o.getReference() == this.getReference()) {
            return 0;
        } else if (o.getReference() > this.getReference()) {
            return -1;
        } else {
            return 1;
        }
    }


    // ************ GETTER ************
    public int getIdProduit(){
        return this.idProduit;
    }
    public int getIdTypeProduit(){
        return this.idTypeProduit;
    }
    public int getIdMarque(){
        return this.idMarque;
    }
    public String getNomProduit(){
        return this.nomProduit;
    }
    public boolean getDisponible(){
        return this.disponible;
    }
    public String getImageProduit(){
        return this.imageProduit;
    }
    public int getReference(){
        return this.reference;
    }
    public int getQuantite(){
        return this.quantite;
    }

    // ************ SETTER ************
    public void setIdProduit(int id){
        this.idProduit = id;
    }
    public void setIdTypeProduit(int id){
        this.idTypeProduit = id;
    }
    public void setIdMarque(int id){
        this.idMarque = id;
    }
    public void setNomProduit(String nom){
        this.nomProduit = nom;
    }
    public void setDisponible(boolean disp){
        this.disponible = disp;
    }
    public void setImageProduit(String image){
        this.imageProduit = image;
    }
    public void setReference(int ref){
        this.reference = ref;
    }
    public void setQuantite(int qte){
        this.quantite = qte;
    }
}
