package com.albasheep.statbde.ClassModel;

/**
 * Created by Alex Chopard on 02/11/2017.
 */

public class Stock {
    private int idProduit;
    private int quantite;

    public Stock(){

    }

    // ************ GETTER ************
    public int getIdProduit(){
        return this.idProduit;
    }
    public int getQuantite(){
        return this.quantite;
    }

    // ************ SETTER ************
    public void setIdProduit(int id){
        this.idProduit = id;
    }
    public void setQuantite(int qte){
        this.quantite = qte;
    }
}
