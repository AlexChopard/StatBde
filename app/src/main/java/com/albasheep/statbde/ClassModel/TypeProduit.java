package com.albasheep.statbde.ClassModel;

/**
 * Created by Alex Chopard on 26/10/2017.
 */

public class TypeProduit {
    private int idTypeProduit;
    private String nomTypeProduit;

    public TypeProduit(){
    }

    public TypeProduit(int id, String nom){
        this.setIdTypeProduit(id);
        this.setNomTypeProduit(nom);
    }


    // ************ GETTER ************
    public int getIdTypeProduit(){
        return this.idTypeProduit;
    }
    public String getNomTypeProduit(){
        return this.nomTypeProduit;
    }

    // ************ SETTER ************
    public void setIdTypeProduit(int id){
        this.idTypeProduit = id;
    }
    public void setNomTypeProduit(String nom){
        this.nomTypeProduit = nom;
    }
}
