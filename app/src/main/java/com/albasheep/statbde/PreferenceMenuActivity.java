package com.albasheep.statbde;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;


/**
 * Created by AlexChop on 14/01/2018.
 */

public class PreferenceMenuActivity extends AppCompatActivity {

    public static String PREF_NAME = "mespreference";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new PreferenceFragementMenu()).commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_preference, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_preference :
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class PreferenceFragementMenu extends PreferenceFragment
    {
        public static boolean statuafficheimage = false;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preference);

            CheckBoxPreference pref = (CheckBoxPreference) findPreference("cbafficheimage");
            statuafficheimage = pref.isChecked();

        }

        public static boolean afficheImage(){
            return statuafficheimage;
        }
    }
}
