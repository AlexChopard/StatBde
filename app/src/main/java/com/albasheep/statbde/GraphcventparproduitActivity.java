package com.albasheep.statbde;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.albasheep.statbde.ClassModel.Produit;

import java.lang.ref.WeakReference;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
/**
 * Created by Alex Chopard on 27/11/2017.
 */

public class GraphcventparproduitActivity extends Activity {

    TextView erreur;
    Spinner spinner, spinner2;
    Button btn_afficherstat;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graphventparproduit);

        erreur = (TextView) findViewById(R.id.erreur);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        btn_afficherstat = (Button) findViewById(R.id.btn_affiche_stat);

        LoadProduitTask load2 = new LoadProduitTask(GraphcventparproduitActivity.this);
        load2.execute();

        btn_afficherstat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String nomPdt = spinner.getSelectedItem().toString();
            String nomPdt2 = spinner2.getSelectedItem().toString();

            LoadStatTask load = new LoadStatTask(GraphcventparproduitActivity.this, nomPdt, nomPdt2);
            load.execute();
            }
        });


    }

    public void loadGraphPage(String produit, ArrayList<Integer> lesSemaines, ArrayList<Integer> lesQuantite, String produit2, ArrayList<Integer> lesSemaines2, ArrayList<Integer> lesQuantite2){
        if(!(produit.compareTo("-- aucun --") == 0 && produit2.compareTo("-- aucun --") == 0)) {
            Intent intent = new Intent(GraphcventparproduitActivity.this, StatNbVenteParProduitActivity.class);

            // Si les deux produit selecionner on pas les même semaine de vente, on armonise et on décale pour qu'il ai les même semane de vente
            if(produit.compareTo("-- aucun --") != 0 && produit2.compareTo("-- aucun --") != 0){
                if(lesSemaines.get(0) < lesSemaines2.get(0)){
                    int diff = lesSemaines2.get(0) - lesSemaines.get(0);
                    for(int i = 0; i < diff; i++){
                        lesQuantite2.add(0, 0);
                        lesQuantite2.remove(lesQuantite2.size() - 1);
                    }
                } else
                    if(lesSemaines.get(0) > lesSemaines2.get(0)){
                        int diff = lesSemaines.get(0) - lesSemaines2.get(0);
                        for(int i = 0; i < diff; i++){
                            lesQuantite.add(0, 0);
                            lesQuantite.remove(lesQuantite.size() - 1);
                        }
                }
            }



            intent.putExtra("produit", produit);

            if(produit.compareTo("-- aucun --") != 0){
                intent.putExtra("size", lesQuantite.size());

                for(int i = 0; i < lesQuantite.size(); i++)
                    intent.putExtra("quantite" + i, lesQuantite.get(i));
                for(int i = 0; i < lesSemaines.size(); i++)
                    intent.putExtra("semaine" + i, lesSemaines.get(i));
            }


            intent.putExtra("produit2", produit2);

            if(produit2.compareTo("-- aucun --") != 0){
                intent.putExtra("size2", lesQuantite2.size());

                for(int i = 0; i < lesQuantite2.size(); i++)
                    intent.putExtra("2quantite" + i, lesQuantite2.get(i));
                for(int i = 0; i < lesSemaines2.size(); i++)
                    intent.putExtra("2semaine" + i, lesSemaines2.get(i));

            }

            startActivity(intent);
        } else
            Toast.makeText(this, "Voisir au moins un produit !", Toast.LENGTH_SHORT).show();
    }

    public void loadProduitPage(ArrayList<Produit> lesProduits){
        ArrayList<String> listProduit = new ArrayList<>();

        listProduit.add("-- aucun --");

        for(int i = 0; i < lesProduits.size(); i++)
            listProduit.add(lesProduits.get(i).getNomProduit());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, listProduit);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner2.setAdapter(adapter);
    }

    public void err(String e){
        erreur.setText(e);
    }

    static class LoadProduitTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<GraphcventparproduitActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        ArrayList<Produit> produits;
        String t;

        public LoadProduitTask (GraphcventparproduitActivity pActivity) {
            link(pActivity);
            produits = new ArrayList<>();
            t = "";
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                if(mActivity.get() != null)
                    mActivity.get().loadProduitPage(produits);
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();

                // Récupère tout les produits
                result = state.executeQuery("select * from vProduit");
                resultMeta = result.getMetaData();
                while(result.next()){
                    produits.add(new Produit(Integer.parseInt(result.getObject(1).toString()),
                            Integer.parseInt(result.getObject(2).toString()),
                            Integer.parseInt(result.getObject(3).toString()),
                            result.getObject(4).toString(),
                            Boolean.parseBoolean(result.getObject(5).toString()),
                            result.getObject(6).toString(),
                            Integer.parseInt(result.getObject(7).toString()),
                            0
                    ));
                }

                result.close();
                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (GraphcventparproduitActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }

    static class LoadStatTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<GraphcventparproduitActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        String t;
        ArrayList<Integer> semaine, semaine2;
        ArrayList<Integer> quantite, quantite2;
        String nomProduit, nomProduit2;


        public LoadStatTask (GraphcventparproduitActivity pActivity, String lNomProduit, String lNomProduit2) {
            link(pActivity);
            t = "";
            semaine = new ArrayList<>();
            quantite = new ArrayList<>();
            semaine2 = new ArrayList<>();
            quantite2 = new ArrayList<>();
            this.nomProduit = lNomProduit;
            this.nomProduit2 = lNomProduit2;
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                if(mActivity.get() != null){
                    mActivity.get().loadGraphPage(nomProduit, semaine, quantite, nomProduit2, semaine2, quantite2);
                }
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                if(nomProduit.compareTo("-- aucun --") != 0){
                    state = ConnexionBDD.instance().getConnexion().createStatement();
                    // Récupère les types de produits
                    result = state.executeQuery("select * from vQteVenteParSemaineParproduit" + " " +
                            "WHERE nomproduit = '" + nomProduit + "';");
                    resultMeta = result.getMetaData();

                    while(result.next()){
                        semaine.add((int) Double.parseDouble(result.getObject(2).toString()));
                        quantite.add((int) Double.parseDouble(result.getObject(3).toString()));
                    }

                    if(quantite.size() < 10){
                        while(quantite.size() < 10){
                            quantite.add(0);
                            semaine.add(semaine.get(semaine.size() - 1) + 1);
                        }
                    }
                    result.close();
                    state.close();
                }

                if(nomProduit2.compareTo("-- aucun --") != 0){
                    state = ConnexionBDD.instance().getConnexion().createStatement();
                    // Récupère les types de produits
                    result = state.executeQuery("select * from vQteVenteParSemaineParproduit" + " " +
                            "WHERE nomproduit = '" + nomProduit2 + "';");
                    resultMeta = result.getMetaData();

                    while(result.next()){
                        semaine2.add((int) Double.parseDouble(result.getObject(2).toString()));
                        quantite2.add((int) Double.parseDouble(result.getObject(3).toString()));
                    }

                    if(quantite2.size() < 10){
                        while(quantite2.size() < 10){
                            quantite2.add(0);
                            semaine2.add(semaine2.get(semaine2.size() - 1) + 1);
                        }
                    }
                    result.close();
                    state.close();
                }


                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (GraphcventparproduitActivity pActivity) {
            mActivity = new WeakReference<GraphcventparproduitActivity>(pActivity);
        }
    }

}
