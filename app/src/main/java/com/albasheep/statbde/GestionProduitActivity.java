package com.albasheep.statbde;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;


/**
 * Created by Alex Chopard on 02/11/2017.
 */

public class GestionProduitActivity extends Activity {

    Button btndisponibiliteproduit, btnajouterproduit, btnreferencementproduit;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(MainActivity.estConnecter)
            setContentView(R.layout.activity_gestionproduit);
        else
            setContentView(R.layout.activity_noconnecter);

        btndisponibiliteproduit = (Button) findViewById(R.id.btn_disponbiliteproduit);
        btnajouterproduit = (Button) findViewById(R.id.btn_ajouterproduit);
        btnreferencementproduit = (Button) findViewById(R.id.btn_referencementproduit);


        btndisponibiliteproduit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GestionProduitActivity.this, DisponibiliteProduitActivity.class);
                startActivity(intent);
            }
        });

        btnajouterproduit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GestionProduitActivity.this, AjouterProduitActivity.class);
                startActivity(intent);
            }
        });

        btnreferencementproduit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GestionProduitActivity.this, ReferencementProduitActivity.class);
                startActivity(intent);
            }
        });
    }

}
