package com.albasheep.statbde;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.CatmullRomInterpolator;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;

/**
 * Created by Alex Chopard on 02/12/2017.
 */

public class StatNbVenteParProduitActivity extends Activity {

    XYPlot plot;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acrivity_statnbventeparproduit);

        Intent i = getIntent();

        plot = (XYPlot) findViewById(R.id.plot);

        final String produit = i.getStringExtra("produit");
        String produit2 = i.getStringExtra("produit2");

        ArrayList<Integer> lesQuantites = new ArrayList<Integer>();
        final ArrayList<Integer> lesSemaine = new ArrayList<Integer>();
        ArrayList<Integer> lesQuantites2 = new ArrayList<Integer>();
        final ArrayList<Integer> lesSemaine2 = new ArrayList<Integer>();

        int size, size2;

        if(produit.compareTo("-- aucun --") != 0){
            size = i.getIntExtra("size", 0);

            for(int y = 0; y < size; y++){
                lesQuantites.add(i.getIntExtra("quantite" + y, -1));
                lesSemaine.add(i.getIntExtra("semaine" + y, -1));
            }

            XYSeries series1 = new SimpleXYSeries(
                    lesQuantites, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, produit);

            LineAndPointFormatter series1Format =
                    new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels);

            series1Format.setInterpolationParams(
                    new CatmullRomInterpolator.Params(10, CatmullRomInterpolator.Type.Uniform));

            plot.addSeries(series1, series1Format);
        }

        if(produit2.compareTo("-- aucun --") != 0){
            size2 = i.getIntExtra("size2", 0);

            for(int y = 0; y < size2; y++){
                lesQuantites2.add(i.getIntExtra("2quantite" + y, -1));
                lesSemaine2.add(i.getIntExtra("2semaine" + y, -1));
            }
            XYSeries series2 = new SimpleXYSeries(
                    lesQuantites2, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, produit2);

            LineAndPointFormatter series2Format =
                    new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels_2);

            series2Format.setInterpolationParams(
                    new CatmullRomInterpolator.Params(10, CatmullRomInterpolator.Type.Uniform));

            plot.addSeries(series2, series2Format);
        }

        plot.setRangeBoundaries(0, 200, BoundaryMode.FIXED);

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                int i = Math.round(((Number) obj).floatValue());
                if(produit.compareTo("-- aucun --") != 0)
                    return toAppendTo.append(lesSemaine.get(i));
                else
                    return toAppendTo.append(lesSemaine2.get(i));
            }
            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });


    }
}
