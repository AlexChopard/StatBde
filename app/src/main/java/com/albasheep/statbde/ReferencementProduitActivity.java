package com.albasheep.statbde;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.albasheep.statbde.ClassModel.Produit;
import com.albasheep.statbde.ClassModel.TypeProduit;

import java.lang.ref.WeakReference;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Alex Chopard on 22/11/2017.
 */

public class ReferencementProduitActivity extends Activity {

    TextView erreur;
    LinearLayout ll;
    Button btnEnregistrer;

    ArrayList<ArrayList<Produit>> lesProduits;
    ArrayList<TypeProduit> lesTypeProduits;
    ArrayList<Spinner> lesSpinenr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referencementproduit);

        erreur = (TextView) findViewById(R.id.erreur);
        ll = (LinearLayout) findViewById(R.id.ll_affichage);
        btnEnregistrer = (Button) findViewById(R.id.btn_validerRef);

        lesSpinenr = new ArrayList<>();
        lesProduits = new ArrayList<>();

        LoadProduitTask loadProduitTask = new LoadProduitTask(ReferencementProduitActivity.this);
        loadProduitTask.execute();

        btnEnregistrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MajRefProduitTask maj = new MajRefProduitTask(ReferencementProduitActivity.this, lesProduits);
                maj.execute();
            }
        });
    }

    public void loadProduitPage(ArrayList<Produit> list_produits, ArrayList<TypeProduit> list_typeproduit){

        lesTypeProduits = list_typeproduit;
        ArrayList<Produit> produitParType;

        // On range les produits par type de produit
        for(int i = 0; i < lesTypeProduits.size(); i++){
            produitParType = new ArrayList<>();
            for(int a = 0; a < list_produits.size(); a ++){
                if(list_produits.get(a).getIdTypeProduit() == lesTypeProduits.get(i).getIdTypeProduit()){
                    produitParType.add(list_produits.get(a));
                }
            }
            lesProduits.add(produitParType);
        }

        setRefAll();



        // On créer le layoutParametre qui est commmun à toute les views
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);

        for(int i = 0; i < lesTypeProduits.size(); i++){
            int nombreDeProduit;
            TextView nomTypeProduit = new TextView(ReferencementProduitActivity.this);
            nomTypeProduit.setText(lesTypeProduits.get(i).getNomTypeProduit());
            nomTypeProduit.setTextSize(30);
            nomTypeProduit.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            // On calcule le nombre de produit du type qu'on à
            nombreDeProduit = lesProduits.get(i).size();

            // On ajoute le nom du Type de Produit
            ll.addView(nomTypeProduit);

            for(int y = 0; y < lesProduits.get(i).size(); y++){

                //On créer un layout hotizontale pour contenir le produit
                LinearLayout llh = new LinearLayout(this);
                llh.setOrientation(LinearLayout.HORIZONTAL);
                llh.setLayoutParams(lp);
                llh.setPadding(10, 20, 10, 10);

                // On créer le text (nom du produit)
                TextView nomProduit = new TextView(ReferencementProduitActivity.this);
                nomProduit.setText(lesProduits.get(i).get(y).getNomProduit());
                nomProduit.setTextSize(15);
                nomProduit.setLayoutParams(lp);

                // On créer le spinner pour le référencement
                Spinner ref = new Spinner(ReferencementProduitActivity.this);

                ArrayList<String> listRef = new ArrayList<>();

                for(int x = 0; x < nombreDeProduit; x++)
                    listRef.add("" + (x + 1));
                ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, listRef);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                ref.setAdapter(adapter);

                ref.setSelection(lesProduits.get(i).get(y).getReference() - 1);
                ref.setId(lesProduits.get(i).get(y).getIdProduit());

                lesSpinenr.add(ref);

                ref.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        for(int i = 0; i < lesProduits.size(); i++){
                            for(int a = 0; a < lesProduits.get(i).size(); a++){
                                if(lesProduits.get(i).get(a).getIdProduit() == parent.getId()){
                                    setRefAll(i, a, Integer.parseInt(parent.getSelectedItem().toString()));
                                    majSpinner();
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                llh.addView(nomProduit);
                llh.addView(ref);

                ll.addView(llh);

            }
        }
    }

    public void majSpinner(){
        for(int i = 0; i < lesSpinenr.size(); i++){
            for(int a = 0; a < lesProduits.size(); a ++){
                for(int b = 0; b < lesProduits.get(a).size(); b++){
                    if(lesSpinenr.get(i).getId() == lesProduits.get(a).get(b).getIdProduit()){
                        lesSpinenr.get(i).setSelection(lesProduits.get(a).get(b).getReference() - 1);
                    }
                }
            }
        }
    }

    public void setRefAll(){

        for(int i = 0; i < lesProduits.size(); i++){
            Collections.sort(lesProduits.get(i));
        }

        for(int i = 0; i < lesProduits.size(); i++){
            for (int a = 0; a < lesProduits.get(i).size(); a ++){
                lesProduits.get(i).get(a).setReference(a + 1);
            }
        }
    }

    public void setRefAll(int indiceTypeP, int indice, int ref){
        int indicemax = -1;
        int a;

        for(int i = 0; i < lesProduits.get(indiceTypeP).size(); i++){
            if(lesProduits.get(indiceTypeP).get(i).getReference() == ref){
                indicemax = i;
                i = lesProduits.get(indiceTypeP).size();
            }
        }

        a = (indice < indicemax) ? 1 : -1 ;

        for(int i = indice + a; i != indicemax + a; i += a){
            lesProduits.get(indiceTypeP).get(i).setReference(lesProduits.get(indiceTypeP).get(i).getReference() - a);

        }

        lesProduits.get(indiceTypeP).get(indice).setReference(ref);

        setRefAll();
    }

    public void retour(){
        this.finish();
    }

    public void err(String e){
        erreur.setText(e);
    }

    static class LoadProduitTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<ReferencementProduitActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        ArrayList<Produit> produits;
        ArrayList<TypeProduit> typeProduits;
        String t;


        LoadProduitTask (ReferencementProduitActivity pActivity) {
            link(pActivity);
            produits = new ArrayList<>();
            typeProduits = new ArrayList<>();
            t = "";
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                if(mActivity.get() != null){
                    mActivity.get().loadProduitPage(produits, typeProduits);
                }
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();

                // Récupère tout les produits
                result = state.executeQuery("select * from vProduit;");
                resultMeta = result.getMetaData();
                while(result.next()){
                    produits.add(new Produit(Integer.parseInt(result.getObject(1).toString()),
                            Integer.parseInt(result.getObject(2).toString()),
                            Integer.parseInt(result.getObject(3).toString()),
                            result.getObject(4).toString(),
                            Boolean.parseBoolean(result.getObject(5).toString()),
                            result.getObject(6).toString(),
                            Integer.parseInt(result.getObject(7).toString()),
                            0
                    ));
                }

                result = state.executeQuery("select * from vTypeProduit");
                resultMeta = result.getMetaData();
                while(result.next()){
                    typeProduits.add(new TypeProduit(Integer.parseInt(result.getObject(1).toString()), result.getObject(2).toString()));
                }

                result.close();
                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (ReferencementProduitActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }

    static class MajRefProduitTask extends AsyncTask<Void, Integer, Boolean> {
        // Référence faible à l'activité
        private WeakReference<ReferencementProduitActivity> mActivity = null;
        Statement state;
        ResultSet result;
        ResultSetMetaData resultMeta;
        String t;
        ArrayList<ArrayList<Produit>> produits;


        MajRefProduitTask (ReferencementProduitActivity pActivity, ArrayList<ArrayList<Produit>> pro) {
            link(pActivity);
            t = "";
            produits = pro;
        }

        @Override
        protected void onPreExecute () {
            // Au lancement de la task
        }

        @Override
        protected void onPostExecute (Boolean result) {
            // Après le lancement de la task
            if(result){
                if(mActivity.get() != null){
                    Toast.makeText(mActivity.get(), "Les références sont à jours !",Toast.LENGTH_SHORT).show();
                    mActivity.get().retour();
                }
            }else{
                // Si il y a un problème on affiche le problème
                if(mActivity.get() != null)
                    mActivity.get().err(t);
            }
        }

        @Override
        protected Boolean doInBackground (Void... arg0) {
            try {
                state = ConnexionBDD.instance().getConnexion().createStatement();


                for(int i = 0; i < produits.size(); i++){
                    for(int a = 0; a < produits.get(i).size(); a++){
                        result = state.executeQuery("select MajRefProduit(" + produits.get(i).get(a).getIdProduit() + ", " +
                                produits.get(i).get(a).getReference() + ");");
                        resultMeta = result.getMetaData();
                    }
                }

                result.close();
                state.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                t = e.toString();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate (Integer... prog) {
            // À chaque avancement du téléchargement, on met à jour la boîte de dialogue
        }

        @Override
        protected void onCancelled () {
        }

        public void link (ReferencementProduitActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }
    }
}
