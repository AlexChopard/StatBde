StatBde est une application mobile de gestion de stock pour le BDE Odin.

L'application cert à :

	-> rentrer les ventes effectuées
	-> ajouter des nouveaux produits
	-> gérer la disponibilité des produits
	-> ajouter des stocks
	-> consulter les stocks
	-> voir des statistiques de ventes


(L'application est en cours de développement, elle contient encore des 'bugs' et certaines parties du code sont brouillon).


Application développé pour le BDE Odin par Alex CHOPARD en tant que membre actif de l'association. 